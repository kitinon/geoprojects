package com.hiqfood.geo;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.hiqfood.geo.ThaiArea;

public class DbToFile {
	static private final String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=CATM;Min Pool Size=2;integratedSecurity=true;";
	static private Connection con;

	static {
		try {
			con = DriverManager.getConnection(connectionUrl);
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println("Fail to initialize MSSQL JDBC driver.  Program aborted.");
			e.printStackTrace();
		}
	}

	private static void loadFromDatabase() throws SQLException {
		Statement stmt;
		stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT code, name, amphoe, changwat, zipcode from tambon");
		while (rs.next()) {
			String code = rs.getString(1);
			String tambon = rs.getString(2);
			String amphoe = rs.getString(3);
			String changwat = rs.getString(4);
			String zipcode = rs.getString(5);
			ThaiArea.add(code, changwat, amphoe, tambon, zipcode);
		}
	}
	
	private static void saveToFile() throws IOException {
		FileOutputStream fileOut;
		ObjectOutputStream out;
		fileOut = new FileOutputStream("ThaiAreas.ser");
		out = new ObjectOutputStream(fileOut);
		ThaiArea.save(out);
		out.close();
		fileOut.close();
	}
	
	public static void main(String[] args) {
		try {
			loadFromDatabase();
			saveToFile();
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}
}