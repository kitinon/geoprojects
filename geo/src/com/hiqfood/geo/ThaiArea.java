package com.hiqfood.geo;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import anywheresoftware.b4a.BA.ShortName;

@ShortName("ThaiArea")
public class ThaiArea implements Serializable {

	private static final long serialVersionUID = 1237765079609636235L;
	private static List<ThaiArea> areas = null;
	
	//Changwat, Amphoe, Tambon, Zipcode filters
	private static String cFilter;
	private static String aFilter;
	private static String tFilter;
	private static String zFilter;
	
	private String code;
	private String changwat;
	private String amphoe;
	private String tambon;
	private String zipcode;
	
	public String getCode() {
		return code;
	}

	public String getChangwat() {
		return changwat;
	}

	public String getAmphoe() {
		return amphoe;
	}

	public String getTambon() {
		return tambon;
	}

	public String getZipcode() {
		return zipcode;
	}

	static public ThaiArea add(String code, String changwat, String amphoe, String tambon, String zipcode) {
		ThaiArea ta = new ThaiArea();
		ta.code = code;
		ta.changwat = changwat;
		ta.amphoe = amphoe;
		ta.tambon = tambon;
		ta.zipcode = zipcode;
		if (areas == null) areas = new ArrayList<>();  
		areas.add(ta);
		return ta;
	}
	
	static public void save(ObjectOutputStream os) throws IOException {
		os.writeObject(areas);
	}

	@SuppressWarnings("unchecked")
	static public void load(ObjectInputStream is) throws ClassNotFoundException, IOException {
		areas = (List<ThaiArea>) is.readObject();
	}
	
	static public boolean setChangwat(String name) {
		cFilter = name;
		for (ThaiArea ta : areas) {
			if (ta.changwat.equals(cFilter)) return true;
		}
		return false;
	}

	static public boolean setAmphoe(String name) {
		aFilter = name;
		for (ThaiArea ta : areas) {
			if (ta.amphoe.equals(aFilter)) return true;
		}
		return false;
	}

	static public boolean setTambon(String name) {
		tFilter = name;
		for (ThaiArea ta : areas) {
			if (ta.tambon.equals(tFilter)) return true;
		}
		return false;
	}

	static public boolean setZipcode(String name) {
		zFilter = name;
		for (ThaiArea ta : areas) {
			if (ta.zipcode.equals(zFilter)) return true;
		}
		return false;
	}
	
	static public void clearFilter() {
		cFilter = null;
		aFilter = null;
		tFilter = null;
		zFilter = null;
	}
	
	private boolean isMatched() {
		if (cFilter != null && !changwat.equals(cFilter)) return false; 
		if (aFilter != null && !amphoe.equals(aFilter)) return false; 
		if (tFilter != null && !tambon.equals(tFilter)) return false; 
		if (zFilter != null && !zipcode.equals(zFilter)) return false; 
		return true;
	}
	
	static public String[] getChangwats() {
		List<String> result = new ArrayList<>();
		for (ThaiArea ta : areas) {
			if (ta.isMatched()) {
				String name = ta.changwat;
				if (!result.contains(name)) result.add(name);
			}
		}
		return result.toArray(new String[result.size()]);
	}

	static public String[] getAmphoes() {
		List<String> result = new ArrayList<>();
		for (ThaiArea ta : areas) {
			if (ta.isMatched()) {
				String name = ta.amphoe;
				if (!result.contains(name)) result.add(name);
			}
		}
		return result.toArray(new String[result.size()]);
	}

	static public String[] getTambons() {
		List<String> result = new ArrayList<>();
		for (ThaiArea ta : areas) {
			if (ta.isMatched()) {
				String name = ta.tambon;
				if (!result.contains(name)) result.add(name);
			}
		}
		return result.toArray(new String[result.size()]);
	}

	static public String[] getZipcodes() {
		List<String> result = new ArrayList<>();
		for (ThaiArea ta : areas) {
			if (ta.isMatched()) {
				String name = ta.zipcode;
				if (!result.contains(name)) result.add(name);
			}
		}
		return result.toArray(new String[result.size()]);
	}

	static public ThaiArea[] getAreas() {
		List<ThaiArea> result = new ArrayList<>();
		for (ThaiArea ta : areas) {
			if (ta.isMatched() && !result.contains(ta)) result.add(ta);
		}
		return result.toArray(new ThaiArea[result.size()]);
	}

	static public int hasFilter() {
		int res = 0;
		if (cFilter != null) res |= 0b1000;
		if (aFilter != null) res |= 0b0100;
		if (tFilter != null) res |= 0b0010;
		if (zFilter != null) res |= 0b0001;
		return res;
	}
}